# HtmlBuilder

## 项目介绍
- 项目名称：HtmlBuilder
- 所属系列：openharmony的第三方组件适配移植
- 功能：html页面的加载
- 项目移植状态：主功能完成
- 调用差异：原组件用text加载出来的，openharmoney使用webview加载出来的
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release 1.0.0
## 效果演示
![输入图片说明](https://gitee.com/chinasoft2_ohos/html-builder/raw/master/img/html.gif "html.gif")
## 安装教程
 1.在项目根目录下的build.gradle文件中
 ```
allprojects {
   repositories {
       maven {
           url 'https://s01.oss.sonatype.org/content/repositories/releases/'
       }
   }
}
 ```
 2.在demo模块的build.gradle文件中
 ```
 dependencies {
   implementation('com.gitee.chinasoft_ohos:HtmlBuilder:1.0.0')
   ......
}
 ```
 在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件, 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

## 使用说明
```
//加载网页数据
 webView.load(buildDemoHtml(), null, false);
 //实例化
 HtmlBuilder html = new HtmlBuilder();
 //从1到6是字体的大小
 html.h1();
 html.h2();
 html.h3();
 html.h4();
 html.h5();
 html.h6();
 //颜色，内容
 html .font(0xFFCAE682, "HtmlBuilder")
//最后返回所有拼接数据
 html.build();

 ```
## 测试信息

codeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检查通过

当前版本demo功能与原组件基本无差异

## 版本迭代
 - 1.0.0
## 版权和许可信息
```
  Copyright 2016 Jared Rummler

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.