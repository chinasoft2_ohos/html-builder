package com.jaredrummler.htmlbuilder.demo.slice;

import com.jaredrummler.htmlbuilder.demo.ResourceTable;
import com.jaredrummler.ohos.util.HtmlBuilder;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.webengine.WebView;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.utils.IntentConstants;
import ohos.utils.net.Uri;

import java.security.SecureRandom;
import java.util.Locale;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private SecureRandom random = new SecureRandom();
    private EventHandler eventHandler = new EventHandler(EventRunner.getMainEventRunner());
    private Runnable annoyingSubtitleRunnable;
    private String url = "https://github.com/jaredrummler/html-builder";

    @Override
    public void onStart(Intent intent) {
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(Color.getIntColor("#388E3C"));
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        findComponentById(ResourceTable.Id_github).setClickedListener(this);
        Text title = (Text) findComponentById(ResourceTable.Id_title);
        Text builde = (Text) findComponentById(ResourceTable.Id_build);
        title.setFont(Font.DEFAULT_BOLD);
        annoyingSubtitleRunnable = new Runnable() {
            @Override
            public void run() {
                int randomColor = 0xFF000000 + 256 * 256 * random.nextInt(256)
                        + 256 * random.nextInt(256) + random.nextInt(256);
                String format = String.format(Locale.ROOT,"#%06X", (0xFFFFFF & randomColor));
                builde.setTextColor(new Color(Color.getIntColor(format)));
                eventHandler.postTask(this, 500);
            }
        };
        eventHandler.postTask(annoyingSubtitleRunnable, 200);
        WebView webView = (WebView) findComponentById(ResourceTable.Id_webview);
        webView.load(buildDemoHtml(), null, false);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    protected void onBackground() {
        super.onBackground();
    }

    @Override
    protected void onStop() {
        super.onStop();
        eventHandler.removeTask(annoyingSubtitleRunnable);
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private String buildDemoHtml() {
        HtmlBuilder html = new HtmlBuilder();
        html.style("*:not(a){ color:" + html.fromStrToRGB(0xFFBEBEBE) + ";background-color: "
                + html.fromStrToRGB(0x00000000) + "}");
        html.h3("Example Usage");

        html.h4().font("cursive", "Code:").close();
        html.h6().font(0xFFCAE682, "HtmlBuilder")
                .append(' ')
                .font(0xFFD4C4A9, "html")
                .append(' ')
                .font(0xFF888888, "=")
                .append(" ")
                .font(0xFF33B5E5, "new")
                .append(" ")
                .font(0xFFCAE682, "HtmlBuilder")
                .append("()")
                .br();
        html.font(0xFFD4C4A9, "html")
                .append(".strong(")
                .font(0xFF95E454, "\"Strong text\"")
                .append(").br();")
                .br();
        html.font(0xFFD4C4A9, "html")
                .append(".font(")
                .font(0xFFCAE682, "Color")
                .append('.')
                .font(0xFF53DCCD, "RED")
                .append(", ")
                .font(0xFF95E454, "\"This will be red text\"")
                .append(");")
                .br();
        html.font(0xFFCAE682, "text")
                .append(".setText(")
                .font(0xFFD4C4A9, "html")
                .append(".build());")
                .br();
        html.h4().font("cursive", "Result:").close();
        html.h5().strong("Strong text").br().font(Color.RED.getValue(), "This will be red text");

        html.h3("Supported Tags");
        html.h6().append("&lt;a href=&quot;...&quot;&gt;").br();
        html.append("&lt;b&gt;").br();
        html.append("&lt;big&gt;").br();
        html.append("&lt;blockquote&gt;").br();
        html.append("&lt;br&gt;").br();
        html.append("&lt;cite&gt;").br();
        html.append("&lt;dfn&gt;").br();
        html.append("&lt;div align=&quot;...&quot;&gt;").br();
        html.append("&lt;em&gt;").br();
        html.append("&lt;font color=&quot;...&quot; face=&quot;...&quot;&gt;").br();
        html.append("&lt;h1&gt;").br();
        html.append("&lt;h2&gt;").br();
        html.append("&lt;h3&gt;").br();
        html.append("&lt;h4&gt;").br();
        html.append("&lt;h5&gt;").br();
        html.append("&lt;h6&gt;").br();
        html.append("&lt;i&gt;").br();
        html.append("&lt;img src=&quot;...&quot;&gt;").br();
        html.append("&lt;p&gt;").br();
        html.append("&lt;small&gt;").br();
        html.append("&lt;strike&gt;").br();
        html.append("&lt;strong&gt;").br();
        html.append("&lt;sub&gt;").br();
        html.append("&lt;sup&gt;").br();
        html.append("&lt;tt&gt;").br();
        html.append("&lt;u&gt;").br();
        html.append("&ul;u&gt;").br();
        html.append("&li;u&gt;").br();

        html.h3("Links");
        html.h6().p()
                .strong().a("https://twitter.com/jaredrummler", "Twitter").close()
                .append("&nbsp;&nbsp;|&nbsp;&nbsp;")
                .strong().a("https://github.com/jaredrummler", "GitHub").close()
                .close();

        return html.build();
    }

    @Override
    public void onClick(Component component) {
        Intent intents = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withAction(IntentConstants.ACTION_SEARCH)
                .withUri(Uri.parse(url))
                .build();
        intents.setOperation(operation);
        startAbility(intents);
    }
}
